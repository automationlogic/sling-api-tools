# Description
A python library to consume the Sling API, plus some apps to show how to use the library.

# Running:
python get-shift-types.py

This currently outputs lots of debug info; the last line is list of shifts on a per-person, per-location basis in JSON format.

# Further reading
https://api.sling.is/ - the external Sling API documentation
