# A useful jq query

Take the output of the allocated shifts and calculate a count of each type by person:
jq -M 'to\_entries |map({key: .key, value: .value | to\_entries|map({key: .key, value: .value|length })|from\_entries})|from\_entries' shifts.json
