import requests
import json
import getpass


class SlingAPIError(Exception):
    pass


class AuthorizationError(SlingAPIError):
    pass


class InvalidPermissionError(SlingAPIError):
    pass


class TargetExistsError(SlingAPIError):
    pass


class ObjectNotFoundError(SlingAPIError):
    pass

ERRORS_FOR_STATUS = {
        403: TargetExistsError,
        404: ObjectNotFoundError,
        405: InvalidPermissionError
        }


def errorForStatus(status_code):
    """ Give a specific Exception if we can, based on the HTTP status code """
    if status_code in ERRORS_FOR_STATUS:
        return ERRORS_FOR_STATUS[status_code]
    else:
        return SlingAPIError

API_PREFIX = 'https://api.sling.is/v1'


class SlingClient:

    GET_HEADERS = {"accept": "application/json"}
    POST_HEADERS = {"accept": "application/json",
                    "Content-Type": "application/json"}

    token = None
    org_id = None
    user_id = None
    memberships = []

    def lookupGroup(self, id):
        endpoint = '/groups/{}'.format(id)
        group_data = self.rawGet(endpoint)
        return group_data

    def lookupUser(self, id):
        endpoint = '/users/{}'.format(id)
        user_data = self.rawGet(endpoint)
        return user_data

    def checkAccountValidity(self, username):
        data = {"email": username}
        endpoint = '/account'
        try:
            response = self.rawGet(endpoint, params=data)
        except TargetExistsError:  # The account exists
            return True
        else:
            raise  # re-raise whatever error occured

    def retrieveShifts(self, from_date, to_date):
        date_filter = "{}/{}".format(from_date, to_date)
        endpoint = ('/calendar/{org_id}/users/{user_id}?dates={date_range}'
                    .format(org_id=self.org_id, user_id=self.user_id,
                            date_range=date_filter))
        shift_data = self.rawGet(endpoint)
        return shift_data

    def rawGet(self, apiCall, params=None):
        """
        Returns a python object encapsulating a JSON repoonse from an HTTP GET request

        Raises an Exception if the status code is not 200
        """
        url = '{}{}'.format(API_PREFIX, apiCall)
        response = requests.get(url, params=params, headers=self.GET_HEADERS)
        if response.status_code != 200:
            raise errorForStatus(response.status_code)
        data = json.loads(response.text)
        return data

    def init(self):
        self.token = None

    def login(self, username, password=None):
        """
        Logs in the object such that future requests take place as the authenticated user

        Raises AuthorizationError in the event of login failure
        """
        url = API_PREFIX + '/account/login'
        payload = {"email": username, "password": password}
        response = requests.post(url, data=json.dumps(payload),
                                 headers=self.POST_HEADERS)
        obj = json.loads(response.text)
        self.memberships = map(lambda g: dict((k, g[k]) for k in [
            'id', 'name', 'type'] if k in g), obj['user']['groups'])
        self.org_id = obj['org']['id']
        self.user_id = obj['user']['id']
        try:
            self.token = response.headers['authorization']
            self.GET_HEADERS.update({'Authorization': self.token})
            self.POST_HEADERS.update({'Authorization': self.token})
        except KeyError:
            raise AuthorizationError

        return json.loads(response.text)
