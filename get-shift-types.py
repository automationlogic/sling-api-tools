#!/bin/env python
import requests
import json
import getpass
import sys
from SlingClient import *
# import SlingClient


SC = SlingClient()
USER = 'YOUREMAILADDRESSHERE@example.com'
SC.checkAccountValidity(USER)
print("Account valid...")
password = getpass.getpass("Enter your sling password: ")
try: 
    login_response = SC.login(USER, password)
except SlingAPIError:
    print("Invalid credentials.")
    sys.exit(1)


print("Logged in...")


location_groups = filter(lambda g: g['type'] == 'location', SC.memberships)
# print("location groups: {}".format(location_groups))
shifts = SC.retrieveShifts("2019-11-01T00:00:00", "2019-11-30T24:00:00")
print("Retrieved schedules...")

user_cache = {}
group_cache = {}
# for lg in [2324672, 2041435, 1907306]:
for lg in map(lambda g: g['id'], location_groups):
    print("Querying info for group {}".format(lg))
    try:
        group_cache[lg] = SC.lookupGroup(lg)
    except ObjectNotFoundError:
        print("Could not read group info for {}".format(lg))
    else:
        print("... group {} read OK".format(group_cache[lg]['name']))

for gid, g in group_cache.items():
    # print("Adding members for group {}".format(g['name']))
    for m in g['members']:
        # print("  Adding member {}".format(m['name']))
        if m['id'] not in user_cache:
          user_cache[m['id']] = {}
        user_cache[m['id']].update(m)

def transform_shift(s):
    shift = {
        "starttime": s['dtstart'],
        "location": s['location']['id'],
        "user": s['user']['id']
        }
    return shift


print("Transforming schedules...")
user_shift_assignments = {}
# print("Pre transformed shifts: {}".format(shifts))
for s in list(map(transform_shift, list(filter(lambda s: s['type']=='shift', shifts)))):
    print("Looking for {}".format(s['user']))
    # if s['user'] not in user_cache:
    #     print("Not found")
    #     user_cache[s['user']] = SC.lookupUser(s['user'])
    # else:
    #     print("Found")
    un = user_cache[s['user']]['name']
    loc_name = group_cache[s['location']]['name']
    if un not in user_shift_assignments:
        print("Creating shift assignment entry for {}".format(un))
        user_shift_assignments[un] = {}
    if loc_name not in user_shift_assignments[un]:
      user_shift_assignments[un][loc_name] = []
    user_shift_assignments[un][loc_name].append(s['starttime'])
print("Transformed schedules")
print("Shift assignments follow...")
print(json.dumps(user_shift_assignments))
